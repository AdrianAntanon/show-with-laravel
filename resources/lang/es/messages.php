<?php
return[
    'Welcome' => 'Bienvenido',
    'Register' => 'Registro',
    'Name' => 'Nombre',
    'Surname' => 'Apellido',
    'Password' => 'Contraseña',
    'Confirm Password' => 'Repetir contraseña',
    'Already registered?' => '¿Ya tienes cuenta?',
];
