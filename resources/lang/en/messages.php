<?php

return[
    'Welcome' => 'Welcome',
    'Register' => 'Register',
    'Name' => 'Name',
    'Surname' => 'Surname',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password',
    'Already registered?' => 'Already registered?',
];
