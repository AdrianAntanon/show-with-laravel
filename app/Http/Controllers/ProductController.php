<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Items;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {
        $value = empty($request->input('price'))?'0':$request->input('price') ;

        $all = DB::select(DB::raw("select * from products where price > $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 4;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products',$products);
    }

    public function addToCart($id){
        $product = Product::find($id);
        Log::error('error');
        Log::warning('warning');
//        Log::info('info');

        Log::channel('buy')->info('ejemplo' . $id);

        $user = Auth::user()->id;
        $cart_bd = Cart::where("user_id", $user)->first();
        if (empty($cart_bd)){
            $cart_bd = new Cart();
            $cart_bd->user_id = $user;
            $cart_bd->save();
        }

        if(!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if(!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "amount" => 1,
                    "price" => $product->price,
                    "image" => $product->image
                ]
            ];
            $cart_items = new Cart_Items();
            $cart_items->cart_id=$cart_bd->id;
            $cart_items->product_id=$id;
            $cart_items->amount = 1;
            $cart_items->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
            $cart[$id]['amount']++;
            $cart_item =Cart_Items::where([['cart_id', $cart_bd->id], ['product_id', $id]])->first();
            $cart_item->amount=$cart[$id]['amount'];

            $cart_item->update();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "amount" => 1,
            "price" => $product->price,
            "image" => $product->image
        ];

        $cart_items = new Cart_Items();
        $cart_items->cart_id=$cart_bd->id;
        $cart_items->product_id=$id;
        $cart_items->amount = 1;
        $cart_items->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request){
        if($request->id && $request->amount){
            $cart = session()->get('cart');
            $cart[$request->id]["amount"] = $request->amount;
            $user = Auth::user()->id;
            $cart_db = Cart::where('user_id', $user)->first();

            $cart_item = Cart_Items::where([['cart_id', $cart_db->id], ['product_id', $request->id]])->first();
            $cart_item->amount=$request->amount;
            error_log($cart_item);
            $cart_item->update();
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request){
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_bd = Cart::where('user_id', $user)->first();
                $cart_item = Cart_Items::where([['cart_id', $cart_bd->id], ['product_id', $request->id]])->first();
                $cart_item->delete();

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta(){
        $cart = session()->get('cart');
        $products = DB::table('cart_items')->get();
        for ($i=0;$i<$products->count();$i++){
            $id=$products[$i]->product_id;
            $amount=$products[$i]->amount;
            $p=Product::find($products[$i]->product_id);
            if ($amount > $p->stock){
                return Redirect::back()->withErrors(['Alerta: SIN STOCK SUFICIENTE','Alerta: SIN STOCK SUFICIENTE' ]);
            }

        }
        DB::table('cart_items')->where('id', '>=', 0)->delete();
        session()->remove('cart');
        return redirect()->back()->with('success', 'El pago se ha completado');
    }
}
